package bucket

import (
	"context"
	"errors"
	"fmt"
	"jobs/db"
	"jobs/model"
	"log"
	"net/url"
	"os"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

// Bucket ...
type Bucket struct {
	Name   string
	Client *minio.Client
}

func NewBucket(endpoint, bucketname, accessKeyID, accessKeySecret string, clientRegion *string) (*Bucket, error) {
	var err error
	var useSSL = true
	var clientLocation = "us-east-1"
	var nBucket = Bucket{
		Name: bucketname,
	}
	if clientRegion != nil {
		clientLocation = *clientRegion
	}
	// log.Printf("[INFO] Will try to connect to bucket at %s\n", endpoint)
	if nBucket.Client, err = minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, accessKeySecret, ""),
		Secure: useSSL,
		Region: clientLocation,
	}); err != nil {
		return nil, err
	}
	return &nBucket, nil
}

func MustNewBucket() Bucket {
	return Bucket{
		Name:   os.Getenv("bucket-name"),
		Client: mustsetupS3Client(),
	}
}

func mustsetupS3Client() (s3Client *minio.Client) {
	var err error
	var useSSL = true
	var endpoint = os.Getenv("bucket-endpoint")
	var accessKeyID = os.Getenv("access-key")
	var secretAccessKey = os.Getenv("secret-key")
	var clientLocation = os.Getenv("client-region")

	// log.Printf("[INFO] Will try to connect to bucket at %s\n", endpoint)
	if s3Client, err = minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
		Region: clientLocation,
	}); err != nil {
		log.Fatalln(err)
	}
	return
}

func (b *Bucket) MustUpsertBucket(bucketName string, bucketRegion *string) {
	var err error
	var ctx = context.Background()
	var bucketLocation = "eu-central-1"
	if bucketRegion != nil {
		bucketLocation = *bucketRegion
	}
	if err = b.Client.MakeBucket(
		ctx,
		bucketName,
		minio.MakeBucketOptions{Region: bucketLocation},
	); err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := b.Client.BucketExists(ctx, bucketName)
		if errBucketExists == nil && exists {
			log.Printf("[INFO] Bucket %s already exist\n", bucketName)
		} else {
			log.Fatalf("[ERROR] Failed to make bucket : %s\n", err.Error())
		}
	} else {
		log.Printf("[INFO] Successfully created bucket %s\n", bucketName)
	}
}

func GetUserBucketClient(cli *db.PrismaClient, userId string) (*Bucket, error) {
	var (
		err          error
		ctx          = context.Background()
		s3Credential *db.S3CredentialModel
		bucketClient *Bucket
		bucketRegion = "eu-central-1"
	)
	if s3Credential, err = cli.S3Credential.FindUnique(
		db.S3Credential.OwnerID.Equals(userId),
	).Exec(ctx); err != nil {
		return nil, errors.New("internal system error")
	}

	if val, ok := s3Credential.BucketRegion(); val != "" && ok {
		bucketRegion = val

	}

	if bucketClient, err = NewBucket(
		fmt.Sprintf("s3.%s.%s", bucketRegion, "wasabisys.com"),
		s3Credential.BucketName,
		s3Credential.KeyID,
		s3Credential.KeySecret,
		nil,
		// s3Credential.ClientRegion,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	return bucketClient, nil
}

func GetSignedUrl(cli *db.PrismaClient, song *db.SongModel, userId string) (*model.S3SignedURL, error) {
	var (
		err          error
		ctx          = context.Background()
		s3Credential *db.S3CredentialModel
		bucketClient *Bucket
		bucketRegion = "eu-central-1"
		signedURL    *url.URL
	)
	if s3Credential, err = cli.S3Credential.FindUnique(
		db.S3Credential.OwnerID.Equals(userId),
	).Exec(ctx); err != nil {
		return nil, errors.New("internal system error")
	}

	if val, ok := s3Credential.BucketRegion(); val != "" && ok {
		bucketRegion = val

	}

	if bucketClient, err = NewBucket(
		fmt.Sprintf("s3.%s.%s", bucketRegion, "wasabisys.com"),
		s3Credential.BucketName,
		s3Credential.KeyID,
		s3Credential.KeySecret,
		nil,
		// s3Credential.ClientRegion,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	if signedURL, err = bucketClient.Client.PresignedGetObject(
		ctx,
		s3Credential.BucketName,
		song.S3Key,
		72*time.Hour,
		nil,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	return &model.S3SignedURL{
		SignedURL: signedURL.String(),
		ExpireAt:  time.Now().Local().Add(72 * time.Hour),
	}, nil
}
