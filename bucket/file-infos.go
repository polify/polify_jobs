package bucket

import (
	"context"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/cheggaaa/pb"
	"github.com/gabriel-vasile/mimetype"
	"github.com/minio/minio-go/v7"
)

// BucketFileInfos ...
type BucketFileInfos struct {
	Mime           *mimetype.MIME
	FileInfo       fs.FileInfo
	Reader         io.Reader
	FileSize       int64
	LocalFilePath  string
	FileName       string
	BucketFilePath string
	ContentType    string
}

// NewFileInfos ...
func MustNewFileInfos(filePath string) BucketFileInfos {
	var err error
	var mime *mimetype.MIME
	var fileInfo fs.FileInfo
	var reader io.Reader
	var fileSize int64
	var fileName, bucketFpath, contentType string
	var trimPath = os.Getenv("trim-path")

	fileName = filepath.Base(filePath)
	if mime, err = mimetype.DetectFile(filePath); err != nil {
		log.Fatal(err)
	}
	contentType = mime.String()
	if reader, err = os.Open(filePath); err != nil {
		log.Fatal(err)
	}
	if fileInfo, err = os.Stat(filePath); err != nil {
		log.Fatal(err)
	}
	bucketFpath = strings.ReplaceAll(filePath, trimPath, "")
	// get the size
	fileSize = fileInfo.Size()
	return BucketFileInfos{
		Mime:           mime,
		FileInfo:       fileInfo,
		Reader:         reader,
		FileSize:       fileSize,
		LocalFilePath:  filePath,
		FileName:       fileName,
		BucketFilePath: bucketFpath,
		ContentType:    contentType,
	}
}

// FileExist check if file is present in bucket
func (f *BucketFileInfos) FileExist(s3Client *minio.Client, bucketName string) (ok bool) {
	var err error
	var obj *minio.ObjectInfo
	ctx := context.Background()
	if obj, err = s3Client.GetObjectACL(
		ctx,
		bucketName,
		f.BucketFilePath,
	); err != nil {
		log.Printf("[ERROR] Error checking file : %s\n", err.Error())
	}
	ok = obj != nil
	return
}

// MustUploadFile upload file to bucket and panic if error
func (f *BucketFileInfos) MustUploadFile(s3Client *minio.Client, bucketName string) (key string) {
	var err error
	var n minio.UploadInfo
	ctx := context.Background()
	progress := pb.New64(f.FileSize)
	progress.Start()
	if n, err = s3Client.PutObject(
		ctx,
		bucketName,
		f.BucketFilePath,
		f.Reader,
		f.FileSize,
		minio.PutObjectOptions{
			ContentType: f.ContentType,
			Progress:    progress,
		}); err != nil {
		log.Fatalf("[ERROR] Error uploading file : %s\n", err.Error())
	}
	key = n.Key
	progress.Finish()
	return
}

// UploadFile upload file to bucket
func (f *BucketFileInfos) UploadFile(s3Client *minio.Client, bucketName string) (key *string, err error) {
	var n minio.UploadInfo
	ctx := context.Background()
	progress := pb.New64(f.FileSize)
	progress.Start()
	if n, err = s3Client.PutObject(
		ctx,
		bucketName,
		f.BucketFilePath,
		f.Reader,
		f.FileSize,
		minio.PutObjectOptions{
			ContentType: f.ContentType,
			Progress:    progress,
		}); err != nil {
		return
	}
	key = &n.Key
	progress.Finish()
	return
}
