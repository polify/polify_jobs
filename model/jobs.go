package model

import (
	"jobs/db"
	"time"

	"github.com/google/uuid"
)

type PolifyJob interface {
	ID() string
	String() string
	Timeout() time.Duration
	Run() error
}

type JobKind string

const (
	JobKindAll              JobKind = JobKind("all")
	JobKindS3Sync           JobKind = JobKind("sync")
	JobKindCoverImage       JobKind = JobKind("coverimage")
	JobKindArtistCoverImage JobKind = JobKind("artistcoverimage")
	JobKindSongDuration     JobKind = JobKind("songduration")
	JobKindSongGenre        JobKind = JobKind("songgenre")

	SyncJobQueueKey             = "polify-sync-*"
	CoverImageJobQueueKey       = "polify-coverimage-*"
	ArtistCoverImageJobQueueKey = "polify-artistcoverimage-*"
	SongDurationJobQueueKey     = "polify-songduration-*"
	SongGenreJobQueueKey        = "polify-songgenre-*"

	AllJobsQueueKey = "polify-*-*"
)

func (j JobKind) Key() string {
	switch j {
	case JobKindS3Sync:
		return SyncJobQueueKey
	case JobKindCoverImage:
		return CoverImageJobQueueKey
	case JobKindArtistCoverImage:
		return ArtistCoverImageJobQueueKey
	case JobKindSongDuration:
		return SongDurationJobQueueKey
	case JobKindSongGenre:
		return SongGenreJobQueueKey
	case JobKindAll:
		return AllJobsQueueKey
	default:
		return ""
	}
}
func (j JobKind) String() string {
	return string(j)
}

func JobKindFromString(s string) *JobKind {
	var kind *JobKind
	switch s {
	case string(JobKindS3Sync):
		v := JobKindS3Sync
		kind = &v
	case string(JobKindCoverImage):
		v := JobKindCoverImage
		kind = &v
	case string(JobKindArtistCoverImage):
		v := JobKindArtistCoverImage
		kind = &v
	case string(JobKindSongDuration):
		v := JobKindSongDuration
		kind = &v
	case string(JobKindSongGenre):
		v := JobKindSongGenre
		kind = &v
	case string(JobKindAll):
		v := JobKindAll
		kind = &v
	}
	return kind
}

// Job is a generalist job structure
type Job struct {
	ID          uuid.UUID
	Kind        JobKind
	Name        string
	MaxDuration time.Duration
	Infos       []JobInfo
}

// JobInfo contains access to a user bucket datas
type JobInfo struct {
	ID              string  `json:"id"`
	Type            JobKind `json:"-"`
	ForceUpdate     bool    `json:"force_update"`
	BatchSize       *int    `json:"batch_size"`
	UserID          string  `json:"user_id"`
	BucketName      string  `json:"bucket_name"`
	BucketDirectory *string `json:"bucket_directory"`
}

type Tasks interface {
	Verbose() bool
	Type() JobKind
	Do(*db.PrismaClient, *JobInfo) (*int, error)
}
