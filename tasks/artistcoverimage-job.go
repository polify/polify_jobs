package tasks

import (
	"context"
	"errors"
	"jobs/db"
	"jobs/model"
	"log"
	"os"
	"strings"
	"time"

	"github.com/irlndts/go-discogs"
)

type ArtistCoverTask struct {
	IsVerbose bool
}

func (a *ArtistCoverTask) Verbose() bool {
	return a.IsVerbose
}
func (a *ArtistCoverTask) Type() model.JobKind {
	return model.JobKindArtistCoverImage
}

func (a *ArtistCoverTask) Do(tx *db.PrismaClient, j *model.JobInfo) (updated *int, err error) {
	var (
		// cld              *cloudinary.Cloudinary
		artists []db.ArtistModel
		// allreadyUploaded *admin.AssetsResult
		ctx   = context.Background()
		start = time.Now()
		updt  int
		dcgs  discogs.Discogs
	)
	if dcgs, err = discogs.New(&discogs.Options{
		UserAgent: "Polify API",
		Currency:  "EUR",
		Token:     os.Getenv("DISCOGS_API_KEY"),
	}); err != nil {
		log.Printf("[ERROR] Failed to load discogs api : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if artists, err = tx.Artist.FindMany(
		db.Artist.CoverURL.IsNull(),
	).
		Exec(ctx); err != nil {
		log.Printf("[ERROR] Failed to get artists : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	for _, artist := range artists {
		var coverUrl *string
		var prse = strings.Split(artist.Name, "&")[0]
		request := discogs.SearchRequest{Type: "artist", Q: prse}
		search, err := dcgs.Search(request)
		if err != nil {
			log.Printf("[ERROR] Failed to search discogs api : %s\n",
				err.Error())
			if strings.Contains(err.Error(), "429 Too Many Requests") {
				log.Printf("[INFO] Too much request, will sleep\n")
				time.Sleep(10 * time.Second)
			}
			continue
		}
		for _, r := range search.Results {
			if r.CoverImage != "" && !strings.HasSuffix(r.CoverImage, ".gif") {
				coverUrl = &r.CoverImage
				if a.IsVerbose {
					log.Printf("[INFO] Found cover Url %s for %s\n",
						*coverUrl, artist.Name)
				}
				break
			}
		}
		if coverUrl == nil {
			if a.IsVerbose {
				log.Printf("[INFO] No cover image found for '%s'\n",
					artist.Name)
			}
			time.Sleep(1 * time.Second)
			continue
		}
		if _, err = tx.Artist.FindUnique(
			db.Artist.ID.Equals(artist.ID),
		).Update(
			db.Artist.CoverURL.SetIfPresent(coverUrl),
		).Exec(ctx); err != nil {
			log.Printf("[ERROR] failed to update artist : %s\n",
				err.Error())
			continue
		}
		if a.IsVerbose {
			log.Printf("[INFO] Updated artist %s\n", artist.Name)
		}
		updt++
		time.Sleep(10 * time.Second)

	}
	updated = &updt
	log.Printf("[INFO] Finished job '%s' id '%s' in '%s', updated %v\n", j.Type.String(), j.ID, time.Since(start).String(), *updated)
	return
}
