package tasks

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"jobs/bucket"
	"jobs/db"
	"jobs/model"
	"log"
	"os"
	"time"

	"github.com/bogem/id3v2"
	"github.com/cespare/xxhash"
	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api"
	"github.com/cloudinary/cloudinary-go/api/admin"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"github.com/minio/minio-go/v7"
)

type CoverImageTask struct {
	IsVerbose bool
}

func (c *CoverImageTask) Verbose() bool {
	return c.IsVerbose
}

func (c *CoverImageTask) Type() model.JobKind {
	return model.JobKindCoverImage
}

func (c *CoverImageTask) Do(tx *db.PrismaClient, j *model.JobInfo) (updated *int, err error) {
	var (
		client           *bucket.Bucket
		cld              *cloudinary.Cloudinary
		songs            []db.SongModel
		allreadyUploaded *admin.AssetsResult
		ctx              = context.Background()
		start            = time.Now()
		updt             int
	)
	if client, err = bucket.GetUserBucketClient(tx, j.UserID); err != nil {
		log.Printf("[ERROR] Failed to get load user bucket client : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if cld, err = cloudinary.NewFromParams(
		os.Getenv("CLOUDINARY_BUCKET"),
		os.Getenv("CLOUDINARY_ID"),
		os.Getenv("CLOUDINARY_SECRET"),
	); err != nil {
		log.Printf("[ERROR] Failed to load Cloudinary client : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if allreadyUploaded, err = cld.Admin.Assets(ctx, admin.AssetsParams{
		AssetType:  api.Image,
		MaxResults: 400,
	}); err != nil {
		log.Printf("[ERROR] Failed to load existing file(s) : %s\n",
			err.Error())
		// return nil, errors.New("internal system error")
	}
	if allreadyUploaded != nil && c.IsVerbose {
		log.Printf("[INFO] Got %v existing asset(s) on Cloudinary\n", len(allreadyUploaded.Assets))
	}
	if songs, err = tx.Song.FindMany(
		db.Song.OwnerID.Equals(j.UserID),
		db.Song.CoverURL.IsNull(),
		db.Song.HasImageTag.Equals(true),
	).
		With(
			db.Song.Album.Fetch(),
			db.Song.Artist.Fetch(),
			db.Song.Genres.Fetch(),
		).
		OrderBy(
			db.Song.AlbumID.Order(db.ASC),
		).
		Exec(ctx); err != nil {
		log.Printf("[ERROR] Failed to get songs : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}

	for cnt, song := range songs {
		var (
			hasImageTag         *bool
			previousSongAlbumID *string
			currentSongAlbumID  *string
			albumCoverURL       *string
			currentAlbum        *db.AlbumModel
			picture             []byte
			picFname            string
		)
		if j.BatchSize != nil && *j.BatchSize == cnt {
			if c.IsVerbose {
				log.Printf("[INFO] Reaching batchSize %v, end of task\n", *j.BatchSize)
			}
			break
		}
		if c.IsVerbose {
			log.Printf("[INFO] Will handle song %v/%v\n", cnt+1, len(songs))
		}
		if v, ok := song.AlbumID(); ok {
			currentSongAlbumID = &v
		}
		if cnt-1 > 0 {
			if v, ok := songs[cnt-1].AlbumID(); ok {
				previousSongAlbumID = &v
			}
		}
		if previousSongAlbumID != nil && currentSongAlbumID != nil &&
			*previousSongAlbumID == *currentSongAlbumID {
			currentAlbum, err = tx.Album.FindUnique(
				db.Album.ID.EqualsIfPresent(currentSongAlbumID),
			).Exec(ctx)
			if err != nil {
				log.Printf("[ERROR] Failed to get album : %s\n", err.Error())
				continue
			}
		} else if currentSongAlbumID != nil {
			currentAlbum, err = tx.Album.FindUnique(
				db.Album.ID.EqualsIfPresent(currentSongAlbumID),
			).Exec(ctx)
			if err != nil {
				log.Printf("[ERROR] Failed to get album : %s\n", err.Error())
				continue
			}
		}

		obj, err := client.Client.GetObject(ctx, client.Name, song.S3Key, minio.GetObjectOptions{})
		if err != nil {
			log.Printf("[ERROR] Failed to get object '%s' : %s\n", song.S3Key, err.Error())
			continue
		}
		st, err := obj.Stat()
		if err != nil {
			log.Printf("[ERROR] Failed to get object '%s' infos : %s\n", song.S3Key, err.Error())
			continue
		}
		if c.IsVerbose {
			log.Printf("[INFO] Getting obj key '%s'\n", st.Key)
		}
		defer obj.Close()
		tag, err := id3v2.ParseReader(obj, id3v2.Options{Parse: true})
		if err != nil {
			log.Printf("[ERROR] Failed to read tags : %s\n", err.Error())
			continue
		}
		defer tag.Close()

		if currentAlbum != nil {
			if v, ok := currentAlbum.CoverURL(); ok {
				albumCoverURL = &v
			} else {
				pictures := tag.GetFrames("APIC")
				if len(pictures) != 0 {
					pic, ok := pictures[0].(id3v2.PictureFrame)
					if !ok {
						log.Printf("[INFO] Couldn't assert picture frame\n")
						continue
					}
					picFname = fmt.Sprintf("%v-%v", xxhash.Sum64String(song.S3Key), 1)
					picture = pic.Picture
					upl := cld.Upload
					uploadRes, err := upl.Upload(ctx, bytes.NewReader(picture), uploader.UploadParams{
						PublicID: picFname,
						Folder:   os.Getenv("CLOUDINARY_DIRECTORY"),
					})
					if err != nil {
						log.Printf("[ERROR] Failed to upload cover image : %s\n",
							err.Error())
						continue
					}
					albumCoverURL = &uploadRes.SecureURL
					if al, ok := song.Album(); ok && al != nil {
						if c.IsVerbose {
							log.Printf("[INFO] Will update album %s\n", al.ID)
						}
						if _, err = tx.Album.FindUnique(
							db.Album.ID.Equals(al.ID),
						).Update(
							db.Album.CoverURL.Set(uploadRes.SecureURL),
						).
							Exec(ctx); err != nil {
							log.Printf("[ERROR] failed to update album : %s\n",
								err.Error())
						}
					} else if c.IsVerbose {
						log.Printf("[ERROR] Song %s has no album\n", song.ID)
					}
				} else {
					if c.IsVerbose {
						log.Printf("[INFO] No image tag\n")
					}
					val := false
					hasImageTag = &val
				}
			}

		}
		if albumCoverURL == nil && hasImageTag == nil {
			if c.IsVerbose {
				log.Printf("[INFO] Nothing to update for song key '%s'\n",
					song.S3Key)
			}
			continue
		}
		var up *db.SongModel
		if up, err = tx.Song.FindUnique(
			db.Song.ID.Equals(song.ID),
		).Update(
			db.Song.CoverURL.SetIfPresent(albumCoverURL),
			db.Song.HasImageTag.SetIfPresent(hasImageTag),
		).
			Exec(ctx); err != nil {
			log.Printf("[ERROR] failed to update song : %s\n",
				err.Error())
			continue
		}
		if c.IsVerbose {
			log.Printf("[INFO] Updated song %+v\n", up)
		}
		updt++

	}
	updated = &updt
	log.Printf("[INFO] Finished job '%s' id '%s' in '%s', updated %v\n", j.Type.String(), j.ID, time.Since(start).String(), *updated)
	return
}
