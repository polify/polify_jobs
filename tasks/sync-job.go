package tasks

import (
	"context"
	"errors"
	"fmt"
	"jobs/bucket"
	"jobs/db"
	"jobs/model"
	"log"
	"strings"
	"time"

	"github.com/minio/minio-go/v7"
)

type S3SyncTask struct {
	IsVerbose bool
}

func (s *S3SyncTask) Verbose() bool {
	return s.IsVerbose
}

func (s *S3SyncTask) Type() model.JobKind {
	return model.JobKindS3Sync
}

func parseBucketKeys(bucketClient *bucket.Bucket, bucketName, prefix string) (allKeys []string, nSongs []*model.Song, nArtists map[string]*map[string]*int, nAlbums map[string]*int) {

	allKeys = []string{}
	nSongs = []*model.Song{}
	nArtists = map[string]*map[string]*int{}
	nAlbums = map[string]*int{}

	ctx := context.Background()
	for el := range bucketClient.Client.ListObjects(
		ctx,
		bucketName,
		minio.ListObjectsOptions{
			Prefix:       prefix,
			Recursive:    true,
			WithMetadata: true,
		},
	) {
		var parseInfos = strings.ReplaceAll(el.Key, prefix+"/", "")
		var infos = strings.Split(parseInfos, "/")
		allKeys = append(allKeys, el.Key)
		if len(infos) == 3 {
			albumS3Key := prefix + "/" + infos[0] + "/" + infos[1]
			nSongs = append(nSongs, &model.Song{
				Title: infos[2],
				S3Key: el.Key,
				Artist: &model.Artist{
					Name: infos[0],
				},
				Album: &model.Album{
					Name:  infos[1],
					S3Key: albumS3Key,
				},
			})
			if nArtists[infos[0]] == nil {
				defVal := 0
				nArtists[infos[0]] = &map[string]*int{
					infos[1]: &defVal,
				}
			} else {
				el := nArtists[infos[0]]
				val := *el
				if val[infos[1]] == nil {
					defval := 0
					val[infos[1]] = &defval
				} else {
					defVal := *val[infos[1]]
					defVal += 1
					val[infos[1]] = &defVal
				}
			}

			if nAlbums[infos[1]] != nil {
				defVal := *nAlbums[infos[1]]
				defVal += 1
				nAlbums[infos[1]] = &defVal
			} else {
				defVal := 0
				nAlbums[infos[1]] = &defVal
			}
		} else {
			nSongs = append(nSongs, &model.Song{
				Title: parseInfos,
				S3Key: el.Key,
			})
		}
	}
	return
}

func (s *S3SyncTask) Do(tx *db.PrismaClient, j *model.JobInfo) (updated *int, err error) {
	var (
		s3Credential   *db.S3CredentialModel
		bucketClient   *bucket.Bucket
		prefix         string
		res            = model.S3Content{}
		updt           int
		allKeys        = []string{}
		existingTitles = []db.SongModel{}
		removedTitles  = []db.SongModel{}
		nSongs         = []*model.Song{}
		nArtists       = map[string]*map[string]*int{}
		nAlbums        = map[string]*int{}
		ctx            = context.Background()
		start          = time.Now()
	)
	if s3Credential, err = tx.S3Credential.FindUnique(
		db.S3Credential.OwnerID.Equals(j.UserID),
	).Exec(ctx); err != nil {
		log.Printf("[ERROR] failed to get s3 keys : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if bucketClient, err = bucket.NewBucket(
		s3Credential.BucketHost,
		j.BucketName,
		s3Credential.KeyID,
		s3Credential.KeySecret,
		nil,
	); err != nil {
		log.Printf("[ERROR] failed to connect to bucket : %s\n", err.Error())
		return nil, fmt.Errorf("internal system error")
	}
	if j.BucketDirectory != nil {
		prefix = *j.BucketDirectory
	}
	allKeys, nSongs, nArtists, nAlbums = parseBucketKeys(bucketClient, j.BucketName, prefix)
	if s.IsVerbose {
		log.Printf("[INFO] Time to listObjects from bucket : %s\n", time.Since(start).String())
		log.Printf("[INFO] Total artists %v, albums %v & titles %v\n", len(nArtists), len(nAlbums), len(nSongs))
	}
	if existingTitles, err = tx.Song.FindMany(
		db.Song.S3Key.In(allKeys),
	).Exec(ctx); err != nil && err != db.ErrNotFound {
		log.Printf("[ERROR] failed to get existing titles : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if removedTitles, err = tx.Song.FindMany(
		db.Song.Not(db.Song.S3Key.In(allKeys)),
	).Exec(ctx); err != nil && err != db.ErrNotFound {
		log.Printf("[ERROR] failed to get existing titles : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if s.IsVerbose {
		log.Printf("[INFO] Found %v title in db and not in bucket\n", len(removedTitles))
	}
	var clean = []*model.Song{}
	for i := range nSongs {
		exist := false
		for j := range existingTitles {
			if nSongs[i].S3Key == existingTitles[j].S3Key {
				exist = true
				break
			}
		}
		if !exist {
			clean = append(clean, nSongs[i])
		}
	}
	if s.IsVerbose {
		log.Printf("[INFO] Found %v allready existing keys\n", len(nSongs)-len(clean))
	}
	nSongs = clean
	res.Songs = len(nSongs)
	for el := range nSongs {
		if s.IsVerbose {
			log.Printf("[INFO] Handling title %v/%v\n", el, len(nSongs))
		}
		if nSongs[el].Artist == nil && nSongs[el].Album == nil {
			if s.IsVerbose {
				log.Printf("[INFO] Found title Name %s, Key %s\n", nSongs[el].Title, nSongs[el].S3Key)
			}
			var nSong *db.SongModel
			if nSong, err = tx.Song.UpsertOne(
				db.Song.SongOwnerS3KeyUniqueKey(
					db.Song.OwnerID.Equals(j.UserID),
					db.Song.S3Key.Equals(nSongs[el].S3Key),
				),
			).Create(
				db.Song.Owner.Link(db.User.ID.Equals(j.UserID)),
				db.Song.Title.Set(nSongs[el].Title),
				db.Song.Duration.Set(""),
				db.Song.S3Key.Set(nSongs[el].S3Key),
			).Update().
				Exec(ctx); err != nil {
				log.Printf("[ERROR] Error saving song : %s\n", err.Error())
				return nil, errors.New("internal system error")
			}
			if s.IsVerbose {
				log.Printf("[INFO] Created or updated song id %s\n", nSong.ID)
			}
			updt++
		} else {
			var nArtist *db.ArtistModel
			if nArtist, err = tx.Artist.UpsertOne(
				db.Artist.Name.Equals(nSongs[el].Artist.Name),
			).Create(
				db.Artist.Name.Set(nSongs[el].Artist.Name),
			).Update().Exec(ctx); err != nil {
				log.Printf("[ERROR] Error saving artist '%s' : %s\n", nSongs[el].Artist.Name, err.Error())
				return nil, errors.New("internal system error")
			}
			var nAlbum *db.AlbumModel
			if nAlbum, err = tx.Album.UpsertOne(
				db.Album.AlbumOwnerS3KeyUniqueKey(
					db.Album.OwnerID.Equals(j.UserID),
					db.Album.S3Key.Equals(nSongs[el].Album.S3Key),
				),
			).Create(
				db.Album.Owner.Link(db.User.ID.Equals(j.UserID)),
				db.Album.Name.Set(nSongs[el].Album.Name),
				db.Album.S3Key.Set(nSongs[el].Album.S3Key),
				db.Album.Artists.Link(db.Artist.ID.Equals(nArtist.ID)),
			).Update().
				Exec(ctx); err != nil {
				log.Printf("[ERROR] Error saving album : %s\n", err.Error())
				return nil, errors.New("internal system error")
			}
			var nSong *db.SongModel
			if nSong, err = tx.Song.UpsertOne(
				db.Song.SongOwnerS3KeyUniqueKey(
					db.Song.OwnerID.Equals(j.UserID),
					db.Song.S3Key.Equals(nSongs[el].S3Key),
				),
			).Create(
				db.Song.Owner.Link(db.User.ID.Equals(j.UserID)),
				db.Song.Title.Set(nSongs[el].Title),
				db.Song.Duration.Set(""),
				db.Song.S3Key.Set(nSongs[el].S3Key),
				db.Song.Artist.Link(db.Artist.ID.Equals(nArtist.ID)),
				db.Song.Album.Link(db.Album.ID.Equals(nAlbum.ID)),
			).Update().
				Exec(ctx); err != nil {
				log.Printf("[ERROR] Error saving song : %s\n", err.Error())
				return nil, errors.New("internal system error")
			}
			if s.IsVerbose {
				log.Printf("[INFO] Created or updated song id %s\n", nSong.ID)
			}
			updt++
		}
	}
	if len(removedTitles) > 0 {
		var dlres *db.BatchResult
		var idMap = []string{}
		for el := range removedTitles {
			idMap = append(idMap, removedTitles[el].ID)
		}
		if dlres, err = tx.Song.FindMany(
			db.Song.ID.In(idMap),
		).Delete().Exec(ctx); err != nil && err != db.ErrNotFound {
			log.Printf("[ERROR] Error removing deleted song : %s\n", err.Error())
			return nil, errors.New("internal system error")
		}
		if dlres != nil {
			updt += dlres.Count
		}
	}
	var dlres *db.BatchResult
	if dlres, err = tx.Album.FindMany(
		db.Album.Songs.Every(
			db.Song.ID.Equals(""),
		),
	).Delete().Exec(ctx); err != nil && err != db.ErrNotFound {
		log.Printf("[ERROR] Error removing deleted albums : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if dlres != nil {
		if s.IsVerbose {
			log.Printf("[INFO] Removed %v albums with no titles\n", dlres.Count)
		}
		updt += dlres.Count
	}
	if dlres, err = tx.Artist.FindMany(
		db.Artist.Songs.Every(
			db.Song.ID.Equals(""),
		),
	).Delete().Exec(ctx); err != nil && err != db.ErrNotFound {
		log.Printf("[ERROR] Error removing deleted artists : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if dlres != nil {
		if s.IsVerbose {
			log.Printf("[INFO] Removed %v artists with no titles\n", dlres.Count)
		}
		updt += dlres.Count
	}
	res.Artists = len(nArtists)
	res.Albums = len(nAlbums)
	updated = &updt
	log.Printf("[INFO] Finished job '%s' id '%s' in '%s', updated %v\n", j.Type.String(), j.ID, time.Since(start).String(), *updated)
	return
}
