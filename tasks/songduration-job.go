package tasks

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"jobs/bucket"
	"jobs/db"
	"jobs/model"
	"log"
	"os"
	"time"

	"github.com/minio/minio-go/v7"
	accoustid "github.com/zakkor/go-acoustid"
)

type SongDurationTask struct {
	IsVerbose bool
}

func (s *SongDurationTask) Verbose() bool {
	return s.IsVerbose
}

func (s *SongDurationTask) Type() model.JobKind {
	return model.JobKindSongDuration
}

// GetDur use github.com/zakkor/go-acoustid to get an acoustid fingerprint for file fName and to query AcoustID api to find duration data (might panic !)
func getDur(fName string, verbose bool) (duration *string, hasOne *bool) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[ERROR] Recovered in getDur for file '%s' : %v\n", fName, r)
		}
	}()
	fp := accoustid.NewFingerprint(fName)
	if verbose {
		log.Printf("[INFO] Got FP, Will query AcoustId api\n")
	}
	acResp := accoustid.MakeAcoustIDRequest(fp)
	if len(acResp.Results) > 0 {
		var ress = acResp.Results[0]
		if len(ress.Recordings) > 0 {
			var rcord = ress.Recordings[0]
			if rcord.Duration > 0 {
				dur, _ := time.ParseDuration(fmt.Sprintf("%vs", rcord.Duration))
				if verbose {
					log.Printf("[INFO] Recording duration %v %s\n", rcord.Duration, dur.String())
				}
				sg := dur.String()
				duration = &sg
			} else {
				if verbose {
					log.Printf("[INFO] No recording duration info for '%s'\n", fName)
				}
				val := false
				hasOne = &val
			}
		} else {
			if verbose {
				log.Printf("[INFO] No recording info for '%s'\n", fName)
			}
			val := false
			hasOne = &val
		}
	} else {
		if verbose {
			log.Printf("[INFO] No AcoustId found for '%s'\n",
				fName)
		}
		val := false
		hasOne = &val
	}
	return
}

func (s *SongDurationTask) Do(tx *db.PrismaClient, j *model.JobInfo) (updated *int, err error) {
	var (
		client      *bucket.Bucket
		songs       []db.SongModel
		ctx         = context.Background()
		start       = time.Now()
		updtd       int
		hasacoustid *bool
	)
	if client, err = bucket.GetUserBucketClient(tx, j.UserID); err != nil {
		return nil, errors.New("internal system error")
	}
	if !j.ForceUpdate {
		val := true
		hasacoustid = &val
	}
	if songs, err = tx.Song.FindMany(
		db.Song.OwnerID.Equals(j.UserID),
		db.Song.Duration.Equals(""),
		db.Song.HasAcoustIDFp.EqualsIfPresent(hasacoustid),
	).
		OrderBy(db.Song.AlbumID.Order(db.ASC)).
		Exec(ctx); err != nil {
		log.Printf("[ERROR] Failed to get songs : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}

	for cnt, song := range songs {
		var (
			songDuration  *string
			hasAcoustIDFp *bool
		)
		if j.BatchSize != nil && *j.BatchSize == cnt {
			if s.IsVerbose {
				log.Printf("[INFO] Reaching batchSize %v, end of task\n", *j.BatchSize)
			}
			break
		}
		if s.IsVerbose {
			log.Printf("[INFO] Will handle song %v/%v\n", cnt+1, len(songs))
		}

		obj, err := client.Client.GetObject(ctx, client.Name, song.S3Key, minio.GetObjectOptions{})
		if err != nil {
			log.Printf("[ERROR] Failed to get object '%s' : %s\n", song.S3Key, err.Error())
			continue
		}
		st, err := obj.Stat()
		if err != nil {
			log.Printf("[ERROR] Failed to get object '%s' infos : %s\n", song.S3Key, err.Error())
			continue
		}
		if s.IsVerbose {
			log.Printf("[INFO] Getting obj key '%s'\n", st.Key)
		}
		defer obj.Close()
		file, err := ioutil.TempFile("./out", "fp")
		if err != nil {
			panic(err)
		}
		if s.IsVerbose {
			log.Printf("[INFO] Create temp file %s\n", file.Name())
		}
		data, _ := ioutil.ReadAll(obj)
		if _, err = file.Write(data); err != nil {
			panic(err)
		}
		songDuration, hasAcoustIDFp = getDur(file.Name(), s.IsVerbose)
		if err = os.Remove(file.Name()); err != nil {
			panic(err)
		}
		if songDuration == nil && hasAcoustIDFp == nil {
			if s.IsVerbose {
				log.Printf("[INFO] Nothing to update for song key '%s'\n",
					song.S3Key)
			}
			continue
		}
		var up *db.SongModel
		if up, err = tx.Song.FindUnique(
			db.Song.ID.Equals(song.ID),
		).Update(
			db.Song.Duration.SetIfPresent(songDuration),
			db.Song.HasAcoustIDFp.SetIfPresent(hasAcoustIDFp),
		).
			Exec(ctx); err != nil {
			log.Printf("[ERROR] failed to update song : %s\n",
				err.Error())
			continue
		}
		if s.IsVerbose {
			log.Printf("[INFO] Updated song %+v\n", up)
		}
		updtd++

	}
	updated = &updtd
	log.Printf("[INFO] Finished job '%s' id '%s' in '%s', updated %v\n", j.Type.String(), j.ID, time.Since(start).String(), *updated)
	return
}
