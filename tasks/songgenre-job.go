package tasks

import (
	"context"
	"errors"
	"fmt"
	"jobs/db"
	"jobs/model"
	"jobs/utils"
	"log"
	"os"
	"strings"
	"time"

	"github.com/irlndts/go-discogs"
)

type SongGenreTask struct {
	IsVerbose bool
}

func (s *SongGenreTask) Verbose() bool {
	return s.IsVerbose
}

func (s *SongGenreTask) Type() model.JobKind {
	return model.JobKindSongGenre
}

func (s *SongGenreTask) Do(tx *db.PrismaClient, j *model.JobInfo) (updated *int, err error) {
	var (
		updt   int
		genres []db.MusicGenreModel
		albums []db.AlbumModel
		ctx    = context.Background()
		start  = time.Now()
		dcgs   discogs.Discogs
	)
	if dcgs, err = discogs.New(&discogs.Options{
		UserAgent: "Polify API",
		Currency:  "EUR",
		Token:     os.Getenv("DISCOGS_API_KEY"),
	}); err != nil {
		log.Printf("[ERROR] Failed to load discogs api : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if genres, err = tx.MusicGenre.FindMany().
		Exec(ctx); err != nil {
		log.Printf("[ERROR] Failed to get music genres : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if albums, err = tx.Album.FindMany(
		db.Album.OwnerID.Equals(j.UserID),
		db.Album.Genres.Every(
			db.MusicGenre.Not(
				db.MusicGenre.ID.GT(0),
			),
		),
	).With(
		db.Album.Artists.Fetch(),
		db.Album.Genres.Fetch(),
	).
		Exec(ctx); err != nil {
		log.Printf("[ERROR] Failed to get songs : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	for cnt, al := range albums {
		if j.BatchSize != nil && *j.BatchSize == cnt {
			if s.IsVerbose {
				log.Printf("[INFO] Reaching batchSize %v, end of task\n", *j.BatchSize)
			}
			break
		}
		time.Sleep(1 * time.Second) // Rate limiting
		albumArtists := al.Artists()
		albumArtist := albumArtists[0].Name
		if s.IsVerbose {
			log.Printf("[INFO] Will handle album %v/%v '%s'\n", cnt+1, len(albums), al.Name)
		}
		// request := discogs.SearchRequest{Type: "release", Q: al.Name, PerPage: 1}
		request := discogs.SearchRequest{Artist: albumArtist, ReleaseTitle: al.Name, PerPage: 1}
		search, err := dcgs.Search(request)
		if err != nil {
			log.Printf("[ERROR] Failed to search discogs api : %s\n",
				err.Error())
			if strings.Contains(err.Error(), "429 Too Many Requests") {
				log.Printf("[INFO] Too much request, will sleep\n")
				time.Sleep(10 * time.Second)
			}
			continue
		} else if len(search.Results) == 0 {
			if s.IsVerbose {
				fmt.Printf("[INFO] No release found via Discog\n")
			}
			continue
		}
		res := search.Results[0]
		for jj := range res.Style {
			res.Genre = utils.AppendIfMissing(res.Genre, res.Style[jj])
		}
		if len(res.Genre) > 0 {
			var existgenreIDS = []int{}
			var tocreategenre = []string{}
			for _, dg := range res.Genre {
				var exist bool
				if s.IsVerbose {
					fmt.Printf("[INFO] Will check if genre '%s' exist\n", dg)
				}
				for el := range genres {
					if genres[el].Name == dg {
						if s.IsVerbose {
							fmt.Printf("[INFO] Found known genre '%s'\n", genres[el].Name)
						}
						existgenreIDS = append(existgenreIDS, genres[el].ID)
						exist = true
					}
				}
				if !exist {
					if s.IsVerbose {
						fmt.Printf("[INFO] Genre '%s' is not found\n", dg)
					}
					tocreategenre = append(tocreategenre, dg)
				}
			}
			for nG := range tocreategenre {
				if s.IsVerbose {
					fmt.Printf("[INFO] Will create genre '%s'\n", tocreategenre[nG])
				}
				nGenre, err := tx.MusicGenre.CreateOne(
					db.MusicGenre.Name.Set(tocreategenre[nG]),
					db.MusicGenre.Type.Set(db.MusicGenreTypeDISCOGS),
				).Exec(ctx)
				if err != nil {
					log.Printf("[ERROR] Failed to create genre : %s\n",
						err.Error())
					return nil, errors.New("internal system error")
				}
				existgenreIDS = append(existgenreIDS, nGenre.ID)
				genres = append(genres, *nGenre)
			}
			toLink := []db.MusicGenreWhereParam{}
			for kk := range existgenreIDS {
				toLink = append(toLink, db.MusicGenre.ID.Equals(existgenreIDS[kk]))
			}
			if _, err = tx.Album.FindUnique(
				db.Album.ID.Equals(al.ID),
			).Update(
				db.Album.Genres.Link(
					toLink...,
				),
			).Exec(ctx); err != nil {
				log.Printf("[ERROR] Failed to update album : %s\n",
					err.Error())
				return nil, errors.New("internal system error")
			}
			updt++
		} else {
			if s.IsVerbose {
				fmt.Printf("[INFO] No genre found for this release\n")
			}
			continue
		}
	}

	updated = &updt
	log.Printf("[INFO] Finished job '%s' id '%s' in '%s', updated %v\n", j.Type.String(), j.ID, time.Since(start).String(), *updated)
	return
}
