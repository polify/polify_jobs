package main

import (
	"bytes"
	"context"
	"encoding/json"
	_ "expvar"
	"flag"
	"fmt"
	"jobs/db"
	"jobs/model"
	"jobs/tasks"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
)

func MustGetJobFromRedis(rdb *redis.Client, jobKind model.JobKind) (jobs []model.JobInfo) {
	var n int
	var err error
	var cursor uint64
	var allKeys []string
	ctx := context.Background()
	jobKey := jobKind.Key()
	jobs = []model.JobInfo{}
	for {
		var synckeys []string
		var err error
		synckeys, cursor, err = rdb.Scan(
			ctx,
			cursor,
			jobKey,
			10,
		).Result()
		if err != nil {
			panic(err)
		}
		n += len(synckeys)
		allKeys = append(allKeys, synckeys...)
		if cursor == 0 {
			break
		}
	}
	for el := range allKeys {
		var res string
		var nInfo model.JobInfo
		if err = rdb.Get(ctx, allKeys[el]).Scan(&res); err != nil {
			fmt.Printf("[ERROR] Invalid job key '%s' : %s\n", allKeys[el], err.Error())
			continue
		}
		jobkeyP := strings.Split(allKeys[el], "-")
		if len(jobkeyP) != 3 {
			fmt.Printf("[ERROR] Invalid job key '%s' %v\n", allKeys[el], len(jobkeyP))
			continue
		}
		dcde := json.NewDecoder(bytes.NewBufferString(res))
		dcde.DisallowUnknownFields()
		if err = dcde.Decode(&nInfo); err != nil {
			fmt.Printf("[ERROR] Invalid job key '%s' %s\n", allKeys[el], err.Error())
			continue
		}
		nInfo.ID = jobkeyP[2]
		var kind *model.JobKind
		if kind = model.JobKindFromString(jobkeyP[1]); kind == nil {
			fmt.Printf("[ERROR] Invalid job key '%s', skipping\n", jobkeyP[1])
			continue
		}
		nInfo.Type = *kind
		jobs = append(jobs, nInfo)
	}
	log.Printf("[INFO] Found %v Jobs in '%s' queue\n", len(jobs), jobKind.String())
	return
}

func main() {
	var err error
	var stageVal = "dev"
	var stageVar = os.Getenv("STAGE")
	var verboseFlag = flag.Bool("verbose", false, "set verbosity or not")
	var stageFlag = flag.String("stage", "dev", "3 stages : dev, preprod & prod")
	var sleepFlag = flag.Duration("sleep", 2*time.Hour, "intervall to wait before picking new jobs")
	var typeFlag = flag.String("type", "all", "4 types :\n\t'sync' s3 bucket content to your collection.\n\t'coverimage' to load all id3v2 tag embedded cover image.\n\t'songduration' to get song duration from acoustid fingerprint\n\t'songgenre' to get song genre from discog api search\n")

	flag.Parse()
	if *stageFlag == "dev" && stageVar != "" {
		stageVal = stageVar
	} else {
		stageVal = *stageFlag
	}
	envfName := "." + stageVal + ".env"
	if err = godotenv.Load(envfName); err != nil {
		log.Printf("[INFO] No '%s' file to load\n", envfName)
	} else {
		log.Printf("[INFO] '%s' file loaded\n", envfName)
	}
	if os.Getenv("VERBOSE") != "" {
		tr := true
		verboseFlag = &tr
	}
	client := db.NewClient()
	if err = client.Prisma.Connect(); err != nil {
		err = fmt.Errorf("prisma: cannot connect: %s", err.Error())
		panic(err)
	}
	var redisUrl = os.Getenv("REDIS_URL")
	var redisTlsUrl = os.Getenv("REDIS_TLS_URL")
	if redisTlsUrl != "" {
		redisUrl = redisTlsUrl
	}
	opt, err := redis.ParseURL(redisUrl)
	if err != nil {
		panic(err)
	}
	if opt.TLSConfig != nil {
		opt.TLSConfig.InsecureSkipVerify = true
	}
	rdb := redis.NewClient(opt)
	ctx := context.Background()
	if err = rdb.Ping(ctx).Err(); err != nil {
		err = fmt.Errorf("redis: cannot ping: %s", err.Error())
		panic(err)
	}
	os.RemoveAll("./out")
	if err = os.MkdirAll("./out", os.ModePerm); err != nil {
		panic(err)
	}
	var kind = model.JobKindFromString(*typeFlag)
	var methods = map[model.JobKind]model.Tasks{
		model.JobKindS3Sync: &tasks.S3SyncTask{
			IsVerbose: *verboseFlag,
		},
		model.JobKindArtistCoverImage: &tasks.ArtistCoverTask{
			IsVerbose: *verboseFlag,
		},
		model.JobKindCoverImage: &tasks.CoverImageTask{
			IsVerbose: *verboseFlag,
		},
		model.JobKindSongDuration: &tasks.SongDurationTask{
			IsVerbose: *verboseFlag,
		},
		model.JobKindSongGenre: &tasks.SongGenreTask{
			IsVerbose: *verboseFlag,
		},
	}
	if kind == nil {
		log.Fatalf("[ERROR] unknow job type '%s'", *typeFlag)
	}
	for {
		var wg sync.WaitGroup
		jobs := MustGetJobFromRedis(rdb, *kind)
		start := time.Now()
		for el := range jobs {
			wg.Add(1)
			log.Printf(
				"[INFO] Will run '%s' job id '%s' for '%s' %v on %v\n",
				string(jobs[el].Type),
				jobs[el].ID,
				jobs[el].BucketName,
				el+1,
				len(jobs),
			)
			i := el
			kind := jobs[el].Type
			go func() {
				defer wg.Done()
				// var updated *int
				if _, err = methods[kind].Do(client, &jobs[i]); err != nil {
					panic(err)
				}
				// if updated != nil {
				// 	log.Printf("[INFO] Updated %v item for job id '%s'\n", *updated, jobs[i].ID)
				// }
			}()
		}
		wg.Wait()
		log.Printf("[INFO] All jobs done in '%s'\n", time.Since(start).String())
		log.Printf("[INFO] Will take more jobs at %s", time.Now().Add(*sleepFlag).Format(time.RFC3339))
		time.Sleep(*sleepFlag)
	}
}
