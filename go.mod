module jobs

go 1.16

require (
	github.com/bogem/id3v2 v1.2.0
	github.com/cespare/xxhash v1.1.0
	github.com/cheggaaa/pb v1.0.29
	github.com/cloudinary/cloudinary-go v1.2.0
	github.com/gabriel-vasile/mimetype v1.3.1
	github.com/go-redis/redis/v8 v8.11.0
	github.com/google/uuid v1.2.0
	github.com/iancoleman/strcase v0.2.0
	github.com/irlndts/go-discogs v0.3.2
	github.com/joho/godotenv v1.3.0
	github.com/minio/minio-go/v7 v7.0.11
	github.com/prisma/prisma-client-go v0.10.1-0.20210722160847-925231174adf
	github.com/shopspring/decimal v1.2.0
	github.com/takuoki/gocase v1.0.0
	github.com/zakkor/go-acoustid v0.0.0-20201125133755-d806d5c92723
)
