# Polify Jobs
A Golang tool to execute data enrichment jobs from a Redis queue


## Usage 
```
  -sleep duration
        intervall to wait before picking new jobs (default 2h0m0s)
  -stage string
        3 stages : dev, preprod & prod (default "dev")
  -type string
        4 types :
                'sync' s3 bucket content to your collection.
                'coverimage' to load all id3v2 tag embedded cover image.
                'songduration' to get song duration from acoustid fingerprint
                'songgenre' to get song genre from discog api search
         (default "all")
  -verbose
        set verbosity or not
```